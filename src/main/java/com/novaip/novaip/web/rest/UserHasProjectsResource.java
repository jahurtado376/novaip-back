package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.UserHasProjectsService;
import com.novaip.novaip.service.dto.UserHasProjectsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserHasProjectsResource {

    @Autowired
    UserHasProjectsService service;

    @PostMapping("/user-has-projects")
    public ResponseEntity create (@RequestBody UserHasProjectsDTO userHasProjectsDTO){ return service.create(userHasProjectsDTO); }

    @GetMapping("/user-has-projects")
    public Page<UserHasProjectsDTO> read (@RequestParam (value = "pageSize") Integer pageSize,
                                          @RequestParam (value = "pageNumber") Integer pageNumber){
        return service.read(pageSize, pageNumber);
    }

    @PutMapping("/user-has-projects")
    public ResponseEntity update (@RequestBody UserHasProjectsDTO userHasProjectsDTO){
        return service.update(userHasProjectsDTO);
    }

    @DeleteMapping("/user-has-projects/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }
}
