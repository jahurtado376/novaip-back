package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.UsersService;
import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UsersResource {

    @Autowired
    UsersService service;

    @PostMapping("/users")
    public ResponseEntity create (@RequestBody UsersDTO usersDTO){ return service.create(usersDTO);}

    @GetMapping("/users")
    public Page<UsersDTO> read (@RequestParam (value = "pageSize") Integer pageSize,
                                @RequestParam (value = "pageNumber") Integer pageNumber){
        return service.read(pageSize,pageNumber);
    }

    @PutMapping("/users")
    public ResponseEntity update(@RequestBody UsersDTO usersDTO){ return service.update(usersDTO); }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }

    @GetMapping("/users/account")
    public ResponseEntity<LoginInformation> getAccount(@RequestParam(value = "username") String username){
        return new ResponseEntity<>(service.getByEmail(username), HttpStatus.OK);
    }
}
