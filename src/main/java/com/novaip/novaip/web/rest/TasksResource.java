package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.TasksService;
import com.novaip.novaip.service.dto.TasksDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class TasksResource {

    @Autowired
    TasksService service;

    @PostMapping("/tasks")
    public ResponseEntity create (@RequestBody TasksDTO tasksDTO){ return service.create(tasksDTO); }

    @GetMapping("/tasks")
    public Page<TasksDTO> read (@RequestParam(value = "pageSize")Integer pageSize,
                                @RequestParam(value = "pageNumber")Integer pageNumber){
        return service.read(pageSize, pageNumber);
    }

    @PutMapping("/tasks")
    public ResponseEntity update(@RequestBody TasksDTO tasksDTO){
        return service.update(tasksDTO);
    }

    @DeleteMapping("/tasks/{id}")
    public void delete (@PathVariable Long id){ service.delete(id);}
}
