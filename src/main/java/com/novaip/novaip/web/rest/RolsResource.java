package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.RolsService;
import com.novaip.novaip.service.dto.RolsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RolsResource {

    @Autowired
    RolsService service;

    @PostMapping("/rols")
    public ResponseEntity create (@RequestBody RolsDTO rolsDTO){
        return service.create(rolsDTO);
    }

    @GetMapping("/rols")
    public Page<RolsDTO> read (@RequestParam(value = "pageSize")Integer pageSize,
                               @RequestParam(value = "pageNumber")Integer pageNumber){
        return service.read(pageSize, pageNumber);
    }

    @PutMapping("/rols")
    public ResponseEntity update (@RequestBody RolsDTO rolsDTO){
        return service.update(rolsDTO);
    }

    @DeleteMapping("/rols/{id}")
    public void delete (@PathVariable Integer id){
        service.delete(id);
    }
}
