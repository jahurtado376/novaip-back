package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.ProjectsService;
import com.novaip.novaip.service.dto.ProjectsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ProjectsResource {

    @Autowired
    ProjectsService service;

    @PostMapping("/projects")
    public ResponseEntity create (@RequestBody ProjectsDTO projectsDTO) {
        return service.create(projectsDTO);
    }

    @GetMapping("/projects")
    public Page<ProjectsDTO> read (@RequestParam(value = "pageSize")Integer pageSize,
                                   @RequestParam(value = "pageNumber")Integer pageNumber){
        return service.read(pageSize, pageNumber);
    }

    @PutMapping("/projects")
    public ResponseEntity update (@RequestBody ProjectsDTO projectsDTO){
        return service.update(projectsDTO);
    }

    @DeleteMapping("/projects/{id}")
    public void delete (@PathVariable Long id){
        service.delete(id);
    }
}
