package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.UserHasTaskService;
import com.novaip.novaip.service.dto.UserHasTaskDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserHasTaskResource {

    @Autowired
    UserHasTaskService service;

    @PostMapping("/user-has-task")
    public ResponseEntity create (@RequestBody UserHasTaskDTO userHasTaskDTO){
        return service.create(userHasTaskDTO);
    }

    @GetMapping("/user-has-task")
    public Page<UserHasTaskDTO> read (@RequestParam (value = "pageSize") Integer pageSize,
                                      @RequestParam (value = "pageNumber") Integer pageNumber){
        return service.read(pageSize, pageNumber);
    }

    @PutMapping("/user-has-task")
    public ResponseEntity update(@RequestBody UserHasTaskDTO userHasTaskDTO){
        return service.update(userHasTaskDTO);
    }

    @DeleteMapping("/user-has-task/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }
}
