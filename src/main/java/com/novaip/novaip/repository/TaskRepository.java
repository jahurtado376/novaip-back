package com.novaip.novaip.repository;

import com.novaip.novaip.domain.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Tasks, Long> {
}
