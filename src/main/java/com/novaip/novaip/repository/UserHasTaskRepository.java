package com.novaip.novaip.repository;

import com.novaip.novaip.domain.UserHasTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserHasTaskRepository extends JpaRepository<UserHasTask, Long> {
}
