package com.novaip.novaip.repository;

import com.novaip.novaip.domain.UserHasProjects;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserHasProjectsRepository extends JpaRepository<UserHasProjects, Long> {
}
