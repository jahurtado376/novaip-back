package com.novaip.novaip.repository;

import com.novaip.novaip.domain.Projects;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectsRepository extends JpaRepository<Projects, Long> {
}
