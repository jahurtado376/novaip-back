package com.novaip.novaip.repository;

import com.novaip.novaip.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {

    Users findByEmail (String email);
}
