package com.novaip.novaip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NovaipApplication {

	public static void main(String[] args) {
		SpringApplication.run(NovaipApplication.class, args);
	}

}
