package com.novaip.novaip.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Rols {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int idRols;

    @Size(min = 2, max = 50, message = "El tamaño del campo debe de estar entre 2 a 50 caracteres")
    @NotEmpty(message = "El campo es requerido")
    @Column(nullable = false)
    private String description;
}
