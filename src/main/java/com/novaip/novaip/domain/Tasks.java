package com.novaip.novaip.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tasks {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long idTask;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 50, message = "El tamaño del campo debe de estar entre 2 a 50 caracteres")
    @Column(nullable = false)
    private String name;

    @Size(min = 2, max = 300, message = "El tamaño del campo debe de estar entre 2 a 50 caracteres")
    private String description;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 50, message = "El tamaño del campo debe de estar entre 2 a 50 caracteres")
    @Column(unique = true)
    private String alias;

    @Column(name = "state")
    private Boolean state;

    @Column(name = "deleted")
    private Boolean deleted;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
}
