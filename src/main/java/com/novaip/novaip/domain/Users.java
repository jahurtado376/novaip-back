package com.novaip.novaip.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long idUsers;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 11, message = "El tamaño del campo debe de estar entre 6 a 11 caracteres")
    @Column(unique = true)
    private String documentNumber;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 80, message = "El tamaño del campo debe de estar entre 2 a 80 caracteres")
    @Column(nullable = false)
    private String firstName;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 80, message = "El tamaño del campo debe de estar entre 2 a 80 caracteres")
    @Column(nullable = false)
    private String lastName;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 80, message = "El tamaño del campo debe de estar entre 6 a 80 caracteres")
    @Column(nullable = false)
    @Email(message = "El email no tiene la estructura correcta")
    private String email;

    @Size(max = 80, min = 5, message = "El tamaño del campo debe de estar entre 5 a 80 caracteres")
    @Column(name = "password", length = 80)
    private String password;

    @Column(name = "state")
    private Boolean state;

    @Column(name = "deleted")
    private Boolean deleted;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    //relation with rols
    @ManyToMany
    @JoinTable(name = "user_has_rol",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_rol"))
    private Set<Rols> rols = new HashSet<>();
}
