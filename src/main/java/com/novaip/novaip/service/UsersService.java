package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsersDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UsersService {

    ResponseEntity create(UsersDTO usersDTO);

    Page<UsersDTO> read(Integer pageSize, Integer pageNumber);

    ResponseEntity update(UsersDTO usersDTO);

    void delete (Long id);

    LoginInformation getByEmail (String username) throws UsernameNotFoundException;
}
