package com.novaip.novaip.service.dto;

import com.novaip.novaip.domain.Rols;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class LoginInformation implements Serializable {

    @Id
    private Long idUser;

    private String documentNumber;

    private Boolean enabled;

    private String fullName;

    @NotEmpty(message = "El campo es requerido")
    @Email(message = "El email no tiene la estructura correcta")
    private String email;

    private Set<Rols> rols;

    private List<String> authorities;
}
