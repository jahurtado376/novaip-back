package com.novaip.novaip.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
public class ProjectsDTO {

    @Id
    private Long idProjects;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 50, message = "El tamaño del campo debe de estar entre 2 a 50 caracteres")
    private String name;

    @Size(min = 2, max = 300, message = "El tamaño del campo debe de estar entre 2 a 300 caracteres")
    private String description;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 50, message = "El tamaño del campo debe de estar entre 2 a 50 caracteres")
    private String alias;

    private Boolean state;

    private Boolean deleted;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
}
