package com.novaip.novaip.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.novaip.novaip.domain.Projects;
import com.novaip.novaip.domain.Users;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
public class UserHasProjectsDTO {

    @Id
    private Long idUserHasProjects;

    @ManyToOne
    private Users users;

    @ManyToOne
    private Projects projects;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;

}
