package com.novaip.novaip.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.novaip.novaip.domain.Tasks;
import com.novaip.novaip.domain.Users;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
public class UserHasTaskDTO {

    @Id
    private Long idUserHasTask;

    @ManyToOne
    private Users users;

    @ManyToOne
    private Tasks task;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;
}
