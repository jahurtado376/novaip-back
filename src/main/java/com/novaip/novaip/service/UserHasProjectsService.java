package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.UserHasProjectsDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface UserHasProjectsService {

    ResponseEntity create(UserHasProjectsDTO userHasProjectsDTO);

    Page<UserHasProjectsDTO> read(Integer pageSize, Integer pageNumber);

    ResponseEntity update(UserHasProjectsDTO userHasProjectsDTO);

    void delete (Long id);
}
