package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.ProjectsDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface ProjectsService {

    ResponseEntity create(ProjectsDTO projectsDTO);

    Page<ProjectsDTO> read(Integer pageSize, Integer pageNumber);

    ResponseEntity update(ProjectsDTO projectsDTO);

    void delete (Long id);

}
