package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.RolsDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface RolsService {

    ResponseEntity create(RolsDTO rolsDTO);

    Page<RolsDTO> read(Integer pageSize, Integer pageNumber);

    ResponseEntity update(RolsDTO rolsDTO);

    void delete (Integer id);
}
