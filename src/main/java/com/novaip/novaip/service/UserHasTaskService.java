package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.UserHasTaskDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface UserHasTaskService {

    ResponseEntity create(UserHasTaskDTO userHasTaskDTO);

    Page<UserHasTaskDTO> read(Integer pageSize, Integer pageNumber);

    ResponseEntity update(UserHasTaskDTO userHasTaskDTO);

    void delete (Long id);
}
