package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Rols;
import com.novaip.novaip.service.dto.RolsDTO;

public class RolsTransformer {

    public static Rols getRolsFromRolsDTO (RolsDTO dto){

        if (dto == null){
            return null;
        }

        Rols rols = new Rols();

        rols.setIdRols(dto.getIdRols());
        rols.setDescription("ROLE_" + dto.getDescription().toUpperCase());

        return rols;
    }

    public static RolsDTO getRolsDTOFromRols (Rols rols){

        if (rols == null){
            return null;
        }

        RolsDTO dto = new RolsDTO();

        dto.setIdRols(rols.getIdRols());
        dto.setDescription(rols.getDescription().toUpperCase());

        return dto;
    }
}
