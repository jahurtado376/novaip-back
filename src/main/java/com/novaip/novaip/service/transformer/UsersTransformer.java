package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Users;
import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsersDTO;

public class UsersTransformer {

    public static Users UsersFromUsersDTO (UsersDTO dto){

        if (dto == null){
            return null;
        }

        Users users = new Users();

        users.setIdUsers(dto.getIdUsers());
        users.setDocumentNumber(dto.getDocumentNumber());
        users.setFirstName(dto.getFirstName().toUpperCase());
        users.setLastName(dto.getLastName().toUpperCase());
        users.setEmail(dto.getEmail());
        users.setPassword(dto.getPassword());
        users.setState(dto.getState());
        users.setDeleted(dto.getDeleted());
        users.setCreationDate(dto.getCreationDate());
        users.setUpdateDate(dto.getUpdateDate());
        users.setRols(dto.getRols());

        return users;
    }

    public static UsersDTO UsersDTOFromUsers (Users users){

        if (users == null){
            return null;
        }

        UsersDTO dto = new UsersDTO();

        dto.setIdUsers(users.getIdUsers());
        dto.setDocumentNumber(users.getDocumentNumber());
        dto.setFirstName(users.getFirstName());
        dto.setLastName(users.getLastName());
        dto.setEmail(users.getEmail());
        dto.setPassword(users.getPassword());
        dto.setState(users.getState());
        dto.setDeleted(users.getDeleted());
        dto.setCreationDate(users.getCreationDate());
        dto.setUpdateDate(users.getUpdateDate());
        dto.setRols(users.getRols());

        return dto;
    }

    public static LoginInformation getLoginInformationFromUsers(Users users){
        if (users == null){
            return null;
        }

        LoginInformation dto = new LoginInformation();

        dto.setIdUser(users.getIdUsers());
        dto.setDocumentNumber(users.getDocumentNumber());
        dto.setFullName(users.getFirstName()+" "+users.getLastName());
        dto.setEnabled(users.getState());
        dto.setEmail(users.getEmail());
        dto.setRols(users.getRols());

        return dto;
    }
}
