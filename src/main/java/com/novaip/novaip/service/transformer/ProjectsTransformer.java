package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Projects;
import com.novaip.novaip.service.dto.ProjectsDTO;

public class ProjectsTransformer {

    public static ProjectsDTO getProjectsDTOFromProjects(Projects projects) {

        if (projects == null){
            return null;
        }

        ProjectsDTO dto = new ProjectsDTO();

        dto.setIdProjects(projects.getIdProjects());
        dto.setName(projects.getName());
        dto.setDescription(projects.getDescription());
        dto.setAlias(projects.getAlias());
        dto.setState(projects.getState());
        dto.setDeleted(projects.getDeleted());
        dto.setStartDate(projects.getStartDate());
        dto.setEndDate(projects.getEndDate());
        dto.setCreationDate(projects.getCreationDate());
        dto.setUpdateDate(projects.getUpdateDate());

        return dto;
    }

    public static Projects getProjectsFromProjectsDTO(ProjectsDTO dto) {

        if (dto == null){
            return null;
        }

        Projects projects = new Projects();

        projects.setIdProjects(dto.getIdProjects());
        projects.setName(dto.getName().toUpperCase());
        projects.setDescription(dto.getDescription().toUpperCase());
        projects.setAlias(dto.getAlias().toUpperCase());
        projects.setState(dto.getState());
        projects.setDeleted(dto.getDeleted());
        projects.setStartDate(dto.getStartDate());
        projects.setEndDate(dto.getEndDate());
        projects.setCreationDate(dto.getCreationDate());
        projects.setUpdateDate(dto.getUpdateDate());

        return projects;
    }
}
