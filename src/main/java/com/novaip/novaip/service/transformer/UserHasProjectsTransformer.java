package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.UserHasProjects;
import com.novaip.novaip.service.dto.UserHasProjectsDTO;

public class UserHasProjectsTransformer {

    public static UserHasProjects UserHasProjectsFromUserHasProjectsDTO (UserHasProjectsDTO dto){

        if (dto == null){
            return null;
        }

        UserHasProjects userHasProjects = new UserHasProjects();

        userHasProjects.setIdUserHasProjects(dto.getIdUserHasProjects());
        userHasProjects.setProjects(dto.getProjects());
        userHasProjects.setUsers(dto.getUsers());
        userHasProjects.setDate(dto.getDate());

        return userHasProjects;
    }

    public static UserHasProjectsDTO UserHasProjectsDTOFromUserHasProjects (UserHasProjects userHasProjects){

        if (userHasProjects == null){
            return null;
        }

        UserHasProjectsDTO dto = new UserHasProjectsDTO();

        dto.setIdUserHasProjects(userHasProjects.getIdUserHasProjects());
        dto.setProjects(userHasProjects.getProjects());
        dto.setUsers(userHasProjects.getUsers());
        dto.setDate(userHasProjects.getDate());

        return dto;
    }
}
