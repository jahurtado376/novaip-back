package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.UserHasTask;
import com.novaip.novaip.service.dto.UserHasTaskDTO;

public class UserHasTaskTransformer {

    public static UserHasTask UserHasTaskFromUserHasTaskDTO(UserHasTaskDTO dto){

        if (dto == null){
            return null;
        }

        UserHasTask userHasTask = new UserHasTask();

        userHasTask.setIdUserHasTask(dto.getIdUserHasTask());
        userHasTask.setTask(dto.getTask());
        userHasTask.setUsers(dto.getUsers());
        userHasTask.setUsers(dto.getUsers());
        userHasTask.setDate(dto.getDate());

        return userHasTask;
    }

    public static UserHasTaskDTO UserHasTaskDTOFromUserHasTask(UserHasTask userHasTask){

        if (userHasTask == null){
            return null;
        }

        UserHasTaskDTO dto = new UserHasTaskDTO();

        dto.setIdUserHasTask(userHasTask.getIdUserHasTask());
        dto.setTask(userHasTask.getTask());
        dto.setUsers(userHasTask.getUsers());
        dto.setUsers(userHasTask.getUsers());
        dto.setDate(userHasTask.getDate());

        return dto;
    }
}
