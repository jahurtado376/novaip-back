package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Tasks;
import com.novaip.novaip.service.dto.TasksDTO;

public class TaskTransformer {

    public static Tasks TasksFromTasksDTO (TasksDTO dto){

        if (dto == null){
            return null;
        }

        Tasks tasks = new Tasks();

        tasks.setIdTask(dto.getIdTask());
        tasks.setName(dto.getName().toUpperCase());
        tasks.setDescription(dto.getDescription().toUpperCase());
        tasks.setAlias(dto.getAlias().toUpperCase());
        tasks.setState(dto.getState());
        tasks.setDeleted(dto.getDeleted());
        tasks.setStartDate(dto.getStartDate());
        tasks.setEndDate(dto.getEndDate());
        tasks.setCreationDate(dto.getCreationDate());
        tasks.setUpdateDate(dto.getUpdateDate());

        return tasks;
    }

    public static TasksDTO TasksDTOFromTasks (Tasks tasks){

        if (tasks == null){
            return null;
        }

        TasksDTO dto = new TasksDTO();

        dto.setIdTask(tasks.getIdTask());
        dto.setName(tasks.getName());
        dto.setDescription(tasks.getDescription());
        dto.setAlias(tasks.getAlias());
        dto.setState(tasks.getState());
        dto.setDeleted(tasks.getDeleted());
        dto.setStartDate(tasks.getStartDate());
        dto.setEndDate(tasks.getEndDate());
        dto.setCreationDate(tasks.getCreationDate());
        dto.setUpdateDate(tasks.getUpdateDate());

        return dto;
    }
}
