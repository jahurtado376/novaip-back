package com.novaip.novaip.service.imp;

import com.novaip.novaip.repository.ProjectsRepository;
import com.novaip.novaip.service.ProjectsService;
import com.novaip.novaip.service.dto.ProjectsDTO;
import com.novaip.novaip.service.transformer.ProjectsTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ProjectsServiceImp implements ProjectsService {

    @Autowired
    ProjectsRepository repository;

    @Override
    public ResponseEntity create(ProjectsDTO projectsDTO) {

        projectsDTO.setCreationDate(LocalDateTime.now());

        return new ResponseEntity(repository.save(ProjectsTransformer.getProjectsFromProjectsDTO(projectsDTO)), HttpStatus.OK);

    }

    @Override
    public Page<ProjectsDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(ProjectsTransformer::getProjectsDTOFromProjects);
    }

    @Override
    public ResponseEntity update(ProjectsDTO projectsDTO) {
        if(repository.findById(projectsDTO.getIdProjects()).isPresent()){
            projectsDTO.setUpdateDate(LocalDateTime.now());
            return new ResponseEntity(repository.save(ProjectsTransformer.getProjectsFromProjectsDTO(projectsDTO)), HttpStatus.OK);
        }else {
            return new ResponseEntity("El proyecto no existe", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
