package com.novaip.novaip.service.imp;

import com.novaip.novaip.repository.TaskRepository;
import com.novaip.novaip.service.TasksService;
import com.novaip.novaip.service.dto.TasksDTO;
import com.novaip.novaip.service.transformer.TaskTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TasksServiceImp implements TasksService {

    @Autowired
    TaskRepository repository;

    @Override
    public ResponseEntity create(TasksDTO tasksDTO) {
        tasksDTO.setCreationDate(LocalDateTime.now());

        return new ResponseEntity(repository.save(TaskTransformer.TasksFromTasksDTO(tasksDTO)), HttpStatus.OK);
    }

    @Override
    public Page<TasksDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(TaskTransformer::TasksDTOFromTasks);
    }

    @Override
    public ResponseEntity update(TasksDTO tasksDTO) {
        if (repository.findById(tasksDTO.getIdTask()).isPresent()){
            tasksDTO.setUpdateDate(LocalDateTime.now());
            return new ResponseEntity(repository.save(TaskTransformer.TasksFromTasksDTO(tasksDTO)), HttpStatus.OK);
        }else {
            return new ResponseEntity("No existe la tarea", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
