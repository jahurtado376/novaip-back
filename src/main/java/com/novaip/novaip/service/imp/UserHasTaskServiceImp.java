package com.novaip.novaip.service.imp;

import com.novaip.novaip.repository.UserHasTaskRepository;
import com.novaip.novaip.service.UserHasTaskService;
import com.novaip.novaip.service.dto.UserHasTaskDTO;
import com.novaip.novaip.service.transformer.UserHasTaskTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserHasTaskServiceImp implements UserHasTaskService {

    @Autowired
    UserHasTaskRepository repository;

    @Override
    public ResponseEntity create(UserHasTaskDTO userHasTaskDTO) {
        userHasTaskDTO.setDate(LocalDateTime.now());
        return new ResponseEntity(repository.save(UserHasTaskTransformer.UserHasTaskFromUserHasTaskDTO(userHasTaskDTO)), HttpStatus.OK);
    }

    @Override
    public Page<UserHasTaskDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(UserHasTaskTransformer::UserHasTaskDTOFromUserHasTask);
    }

    @Override
    public ResponseEntity update(UserHasTaskDTO userHasTaskDTO) {
        if (repository.findById(userHasTaskDTO.getIdUserHasTask()).isPresent()){
            return new ResponseEntity(repository.save(UserHasTaskTransformer.UserHasTaskFromUserHasTaskDTO(userHasTaskDTO)), HttpStatus.OK);
        }else {
            return new ResponseEntity("La relacion no existe", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
