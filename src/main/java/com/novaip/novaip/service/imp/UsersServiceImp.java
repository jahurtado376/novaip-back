package com.novaip.novaip.service.imp;

import com.novaip.novaip.domain.Users;
import com.novaip.novaip.repository.UsersRepository;
import com.novaip.novaip.service.UsersService;
import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsersDTO;
import com.novaip.novaip.service.transformer.UsersTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class UsersServiceImp implements UsersService {

    private Logger logger = LoggerFactory.getLogger(UsersServiceImp.class);

    @Autowired
    UsersRepository repository;

    @Override
    public ResponseEntity create(UsersDTO usersDTO) {
        usersDTO.setCreationDate(LocalDateTime.now());
        return new ResponseEntity(repository.save(UsersTransformer.UsersFromUsersDTO(usersDTO)), HttpStatus.OK);
    }

    @Override
    public Page<UsersDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(UsersTransformer::UsersDTOFromUsers);
    }

    @Override
    public ResponseEntity update(UsersDTO usersDTO) {
        if (repository.findById(usersDTO.getIdUsers()).isPresent()){
            return new ResponseEntity(repository.save(UsersTransformer.UsersFromUsersDTO(usersDTO)), HttpStatus.OK);
        } else {
            return new ResponseEntity("El usuario no existe", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public LoginInformation getByEmail(String username) throws UsernameNotFoundException {

        Users users = repository.findByEmail(username);

        if (users == null){
            logger.error("Error: Email "+ username + " no existe");
            throw new UsernameNotFoundException("Error: Email "+ username + " no existe");
        }
        LoginInformation loginInformation = UsersTransformer.getLoginInformationFromUsers(users);

        ArrayList authorities = new ArrayList<String>();
        loginInformation.getRols().forEach(item -> {
            authorities.add(item.getDescription());
        });

        loginInformation.setAuthorities(authorities);

        return loginInformation;
    }
}
