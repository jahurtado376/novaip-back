package com.novaip.novaip.service.imp;

import com.novaip.novaip.repository.RolsRepository;
import com.novaip.novaip.service.RolsService;
import com.novaip.novaip.service.dto.RolsDTO;
import com.novaip.novaip.service.transformer.RolsTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RolsServiceImp implements RolsService {

    @Autowired
    RolsRepository repository;

    @Override
    public ResponseEntity create(RolsDTO rolsDTO) {
        return new ResponseEntity(repository.save(RolsTransformer.getRolsFromRolsDTO(rolsDTO)), HttpStatus.OK);
    }

    @Override
    public Page<RolsDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(RolsTransformer::getRolsDTOFromRols);
    }

    @Override
    public ResponseEntity update(RolsDTO rolsDTO) {
        if (repository.findById(rolsDTO.getIdRols()).isPresent()){
            return new ResponseEntity(repository.save(RolsTransformer.getRolsFromRolsDTO(rolsDTO)), HttpStatus.OK);
        }else {
            return new ResponseEntity("El rol no existe", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
