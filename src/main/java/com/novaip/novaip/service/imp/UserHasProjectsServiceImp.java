package com.novaip.novaip.service.imp;

import com.novaip.novaip.repository.UserHasProjectsRepository;
import com.novaip.novaip.service.UserHasProjectsService;
import com.novaip.novaip.service.dto.UserHasProjectsDTO;
import com.novaip.novaip.service.transformer.UserHasProjectsTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserHasProjectsServiceImp implements UserHasProjectsService {

    @Autowired
    UserHasProjectsRepository repository;

    @Override
    public ResponseEntity create(UserHasProjectsDTO userHasProjectsDTO) {
        userHasProjectsDTO.setDate(LocalDateTime.now());
        return new ResponseEntity(repository.save(UserHasProjectsTransformer.UserHasProjectsFromUserHasProjectsDTO(userHasProjectsDTO)), HttpStatus.OK);
    }

    @Override
    public Page<UserHasProjectsDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(UserHasProjectsTransformer::UserHasProjectsDTOFromUserHasProjects);
    }

    @Override
    public ResponseEntity update(UserHasProjectsDTO userHasProjectsDTO) {
        if (repository.findById(userHasProjectsDTO.getIdUserHasProjects()).isPresent()){
            return new ResponseEntity(repository.save(UserHasProjectsTransformer.UserHasProjectsFromUserHasProjectsDTO(userHasProjectsDTO)), HttpStatus.OK);
        }else {
            return new ResponseEntity("La relacion no existe", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
