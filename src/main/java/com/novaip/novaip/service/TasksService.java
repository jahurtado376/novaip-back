package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.TasksDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface TasksService {

    ResponseEntity create(TasksDTO tasksDTO);

    Page<TasksDTO> read(Integer pageSize, Integer pageNumber);

    ResponseEntity update(TasksDTO tasksDTO);

    void delete (Long id);
}
