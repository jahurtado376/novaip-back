package com.novaip.novaip.auth;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(HttpMethod.GET,"/api/projects").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.POST,"/api/projects").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.PUT,"/api/projects").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.DELETE,"/api/projects").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.GET, "/api/rols").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.POST, "/api/rols").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.PUT, "/api/rols").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.DELETE, "/api/rols").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.GET, "/api/tasks").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.POST, "/api/tasks").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.PUT, "/api/tasks").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.DELETE, "/api/tasks").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.GET, "/api/user-has-projects").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.POST, "/api/user-has-projects").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.PUT, "/api/user-has-projects").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.DELETE, "/api/user-has-projects").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.GET, "/api/user-has-task").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.POST, "/api/user-has-task").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.PUT, "/api/user-has-task").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.DELETE, "/api/user-has-task").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.GET, "/api/users").hasAnyRole("ADMINISTRADOR", "LIDER")
                .antMatchers(HttpMethod.POST, "/api/users").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.PUT, "/api/users").hasAnyRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.DELETE, "/api/users").hasAnyRole("ADMINISTRADOR")
                .anyRequest().authenticated();

    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        config.setAllowCredentials(true);
        config.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter() {
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(corsConfigurationSource()));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

}
